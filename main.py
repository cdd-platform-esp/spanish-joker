from flask import Flask
import random

app = Flask(__name__)


@app.route('/jokes')
def joke_selector():
    with open('jokes.txt') as f:
        lines = f.read().splitlines()
        
    return str(lines[(random.randint(0, len(lines)-1))])


if __name__ == '__main__':
    app.run(host='0.0.0.0', port = '8081')