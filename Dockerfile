FROM rasa/rasa-sdk:latest
MAINTAINER William Galindez Arias
COPY ./actions /app/actions/
COPY ./jokes.txt /app/jokes.txt
COPY  ./requirements.txt /app/requirements.txt
EXPOSE 8000
WORKDIR /app
USER root
RUN pip install -r requirements.txt
USER 1001
ENTRYPOINT [""] 
CMD python -m rasa_sdk --actions actions -vv
