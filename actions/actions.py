from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from prometheus_client import Counter, start_http_server
import random

REQUESTS = Counter('rasa_action_calls', 'Jokes so far')
start_http_server(8000)


class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Hello World!")

        return []


class JokeSelector(Action):
    def name(self) -> Text:
        return "action_joke"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        with open('jokes.txt') as f:
            lines = f.read().splitlines()
        
        message = str(lines[(random.randint(0, len(lines) - 1))])
        dispatcher.utter_message(text=message)
        REQUESTS.inc()
        return []

#if __name__ == '__main__':
    # Start up the server to expose the metrics.
#    start_http_server(5055)
